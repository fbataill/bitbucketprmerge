"""PR Merge

@author:    Fabien Bataille
@created:   29-Aug-2021

TODO: 
+ manage arguments
+ connect to REST API from Bitbucket
+ get merge status from PR 
+ get review status from PR
+ get all commits from PR
"""

from enum import IntEnum
import py_cui
import time
import threading
import logging
import random
import sys


class Status(IntEnum):
    OK = 0
    ON_GOING = 1
    KO = 2

class Item(IntEnum):
    BUILD = 0
    REVIEW= 1
    REBASE= 2

class PRMerge:
    operationThread_ = None
    status_list_ = ['OK', 'ON-GOING', 'KO']
    item_list_ = ['BUILD','REVIEW','REBASE']

    def __init__(self, master: py_cui.PyCUI, prTitle):

        # This is a reference to our top level CUI object
        self.master = master

        self.computedStatus_ = Status.KO
        self.computedColor_ = py_cui.RED_ON_BLACK

        self.mergeBlock_ = self.master.add_scroll_menu('MERGE '+prTitle,      0, 0)
        # commitBlock = self.master.add_scroll_menu('COMMITS',    1, 0, row_span=2)
        # detailsBlock = self.master.add_scroll_menu('Details', 0, 1, row_span=3)

        self.mergeBlock_.add_item_list(self.item_list_)

#        print(self.mergeBlock_.get_item_list()[Item.BUILD])
        
# change color based on item status
        self.mergeBlock_.add_text_color_rule(
            "KO", py_cui.RED_ON_BLACK, 'endswith')
        self.mergeBlock_.add_text_color_rule(
            "ON-GOING", py_cui.YELLOW_ON_BLACK, 'endswith')
        self.mergeBlock_.add_text_color_rule(
            "OK", py_cui.GREEN_ON_BLACK, 'endswith')

        self.mergeBlock_.set_border_color(self.computedColor_)
        self.mergeBlock_.set_focus_border_color(self.computedColor_)

        operation_thread_ = threading.Thread(target=self.pollBitbucket,daemon=True)
        operation_thread_.start()

    def merge(self):
        # call bitbucket merge
        self.mergeBlock_.set_title("Merged !")
        self.master._stopped = True
        sys.exit(0)

    def pollBitbucket(self):
        while (1 != 0):
            self.computedColor_ = self.getComputedStatusColor()
            self.mergeBlock_.set_border_color(self.computedColor_)
            self.mergeBlock_.set_focus_border_color(self.computedColor_)
            self.mergeBlock_._view_items = self.item_list_
            if(self.computedColor_ == py_cui.GREEN_ON_BLACK):
                self.merge()
            time.sleep(1)

    def getComputedStatusColor(self):
        status = self.getPRStatus()
        switch = {
            Status.OK:       py_cui.GREEN_ON_BLACK,
            Status.ON_GOING: py_cui.YELLOW_ON_BLACK,
            Status.KO:       py_cui.RED_ON_BLACK
        }
        return switch.get(status, "Invalid input")

    def getPRStatus(self):
        status = Status.KO
        buildStatus = self.getBuildStatus()
        reviewStatus = self.getReviewStatus()
        rebaseStatus = self.getRebaseStatus()

        if(buildStatus == Status.KO
                or reviewStatus == Status.KO
                or rebaseStatus == Status.KO):
            status = Status.KO
            return(status)
        elif (buildStatus == Status.ON_GOING
              or reviewStatus == Status.ON_GOING
              or rebaseStatus == Status.ON_GOING):
            status = Status.ON_GOING
            return(status)
        else:
            status = Status.OK

        return(status)

    def getBuildStatus(self):
        index = random.randrange(0, 3)
        buildStatus = 'BUILD ' + self.status_list_[index]
        self.item_list_[Item.BUILD] = buildStatus
        return(Status(index))

    def getReviewStatus(self):
        index = random.randrange(0, 3)
        reviewStatus = 'REVIEW ' + self.status_list_[index]
        self.item_list_[Item.REVIEW] = reviewStatus
        return(Status(index))

    def getRebaseStatus(self):
        index = random.randrange(0, 3)
        rebaseStatus = 'REBASE ' + self.status_list_[index]
        self.item_list_[Item.REBASE] = rebaseStatus
        return(Status(index))

# Create the CUI, pass it to the wrapper object, and start it
root = py_cui.PyCUI(1, 1)
root.set_title('PR Merge')
root.enable_logging(logging_level=logging.DEBUG)
root.toggle_unicode_borders()
root.set_refresh_timeout(1)
s = PRMerge(root,"#3226")
root.start()


#        for key in self.master.get_widgets().keys():
#            if isinstance(self.master.get_widgets()[key], py_cui.widgets.Button):
#                self.master.get_widgets()[key].set_color(color)
